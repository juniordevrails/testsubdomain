class Account < ActiveRecord::Base
  has_many :users

  accepts_nested_attributes_for :users

  validates :name, :subdomain, presence: true, uniqueness: true

  after_create { users.take.update_attributes(owner: true) }

end
