class SessionsController < ApplicationController

  def create
    @account = Account.find_by(subdomain: params[:subdomain])
    @user = @account.users.find_by_email(params[:email])

    if @account && @user.authenticate(params[:password])
      session[:user_id] = @user.id

      redirect_to root_url(subdomain: @account.subdomain)
    else
      redirect_to :back
    end
  end

  def login
    redirect_to root_url(subdomain: @account.subdomain) if current_user
  end

  def logout
    reset_session
    redirect_to root_path
  end

  private
  def user_params
    params.require(:user).permit(:name)
  end

end
