class User < ActiveRecord::Base
  has_secure_password

  belongs_to :account

  validates :name, :email, presence: true
  validates_uniqueness_of :email, scope: :account

  validate :validate_users_email, on: :create

  def validate_users_email
    user = User.find_by(email: email, owner: true)
    errors.add(:email, "уже зарегистрирован для другого аккаунта") if user.present?
  end
end
